<?php /**
* Plugin copyArt - config page
*
* @package  PLX
* @version  1.2
* @date 03/02/2019
* @author Cyril MAGUIRE
**/
if(!defined("PLX_ROOT")) exit; ?>
<?php 
    if(!empty($_POST)) {
        # Controle du token du formulaire
        plxToken::validateFormToken($_POST);

        $plxPlugin->setParam("redirect", plxUtils::strCheck($_POST["redirect"]), "string");

        $plxPlugin->saveParams();
        header("Location: parametres_plugin.php?p=copyArt");
        exit;
    }
?>
<style>
    h2 *{vertical-align:middle;}
    section li{display:inline-block;list-style: none;padding:15px;margin:15px;border:1px solid rgba(0,0,0,0.3);border-radius: 3px}
</style>
<h2><on src="<?php echo PLX_PLUGINS;?>copyArt/icon.png"/><?php $plxPlugin->lang("L_CONFIG_TITLE") ?></h2>
<form action="parametres_plugin.php?p=copyArt" method="post" >
    <li>
        <label><?php $plxPlugin->lang('L_REDIRECT');?> : </label>
            <input type="text" name="redirect" value="<?php echo $plxPlugin->getParam("redirect");?>" placeholder="index ou article"/>
            <?php echo plxToken::getTokenPostMethod(); ?>
    </li>

    <br />
    <input type="submit" name="submit" value="<?php $plxPlugin->lang('L_REC');?>"/>
</form>