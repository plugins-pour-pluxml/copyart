<?php 
	$LANG = array(
		'L_COPY'=>'Dupliquer',
        'L_ART_DUPLICATED'=>'Article dupliqué',
        'L_EDIT_BEFORE_PUBLISH'=>' Éditer avant de publier',
        'L_REMINDER' => 'Modifier le titre et supprimer "**Article dupliqué Éditer avant de publier**", modifiera le lien de l\'article lors de l\'enregistrement',
        'L_CONFIG_TITLE'=>'Configuration',
        'L_REDIRECT'=>'Après la copie, rediriger vers l\'index ou vers l\'article ?',
        'L_REC'=>'Enregistrer',
	);