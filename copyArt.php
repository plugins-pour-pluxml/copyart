<?php
/**
* Plugin copyArt
*
* @package  PLX
* @version  1.2
* @date 03/02/19
* @author Cyril MAGUIRE
**/
class copyArt extends plxPlugin {
        private $className;

    public function __construct($default_lang) {
        # appel du constructeur de la classe plxPlugin (obligatoire)
        parent::__construct($default_lang);

        # limite l'acces a l'ecran de configuration du plugin
        # PROFIL_ADMIN , PROFIL_MANAGER , PROFIL_MODERATOR , PROFIL_EDITOR , PROFIL_WRITER
        $this->setConfigProfil(PROFIL_ADMIN);
        

        # Declaration d'un hook (existant ou nouveau)
        $this->addHook('AdminIndexPrepend','AdminIndexPrepend');
        $this->addHook('AdminIndexPagination','AdminIndexPagination');
        $this->addHook('AdminArticlePrepend','AdminArticlePrepend');
        $this->addHook('AdminArticleTop','AdminArticleTop');

        $this->className = __CLASS__;
        
    }

    # Activation / desactivation
    public function OnActivate() {
        # code à executer à l’activation du plugin
    }
    public function OnDeactivate() {
        # code à executer à la désactivation du plugin
    }
    

    ########################################
    # HOOKS
    ########################################


    ########################################
    # AdminIndexPrepend
    ########################################
    # Description:
    public function AdminIndexPrepend(){

        $redirect = $this->getParam('redirect');

        $date_creation = new \DateTime();
        $day = $date_creation->format('d');
        $month = $date_creation->format('m');
        $year = $date_creation->format('Y');
        $time = $date_creation->format('h:i');
        $date = $date_creation->format('Ymdhi');

        $string = <<<END

# Copie des articles selectionnes
if(isset(\$_POST['selection']) AND !empty(\$_POST['sel']) AND (\$_POST['selection']=='copy') AND isset(\$_POST['idArt'])) {

    \$valid = true;
    if(\$aKeys = array_keys(\$plxAdmin->plxGlob_arts->aFiles)) {
        rsort(\$aKeys);
        \$idArt = \$aKeys[0]+1;
    }
    foreach (\$_POST['idArt'] as \$k => \$v) {

        if((\$aFile = \$plxAdmin->plxGlob_arts->query('/^'.\$v.'.(.+).xml$/')) == false) { # Article inexistant
            plxMsg::Error(L_ERR_UNKNOWN_ARTICLE);
            header('Location: index.php');
            exit;
        }

        # On parse et alimente nos variables
        \$result = \$plxAdmin->parseArticle(PLX_ROOT.\$plxAdmin->aConf['racine_articles'].\$aFile['0']);

        \$_POST['title'] = trim(\$result['title']).' **'.\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_ART_DUPLICATED').\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_EDIT_BEFORE_PUBLISH').'**';
        \$_POST['chapo'] = trim(\$result['chapo']);
        \$_POST['content'] =  trim(\$result['content']);
        \$_POST['tags'] =  trim(\$result['tags']);
        \$_POST['author'] = \$result['author'];
        \$_POST['url'] = \$result['url'].'-copyart';
        \$_POST['date_publication_day'] = '$day';
        \$_POST['date_publication_month'] = '$month';
        \$_POST['date_publication_year'] = '$year';
        \$_POST['date_publication_time'] = '$time';
        \$_POST['date_creation_day'] = '$day';
        \$_POST['date_creation_month'] = '$month';
        \$_POST['date_creation_year'] = '$year';
        \$_POST['date_creation_time'] = '$time';
        \$_POST['date_update_day'] = '$day';
        \$_POST['date_update_month'] = '$month';
        \$_POST['date_update_year'] = '$year';
        \$_POST['date_update_time'] = '$time';
        \$_POST['date_update_old'] = '$date';
        \$_POST['catId'] = explode(',', \$result['categorie']);
        \$_POST['catId'][] = 'draft';
        \$_POST['artId'] = str_pad(\$idArt++,4, '0', STR_PAD_LEFT);
        \$_POST['allow_com'] = 0;
        \$_POST['template'] = \$result['template'];
        \$_POST['meta_description']=\$result['meta_description'];
        \$_POST['meta_keywords']=\$result['meta_keywords'];
        \$_POST['title_htmltag'] = \$result['title_htmltag'];
        \$_POST['thumbnail'] = \$result['thumbnail'];
        \$_POST['thumbnail_title'] = \$result['thumbnail_title'];
        \$_POST['thumbnail_alt'] = \$result['thumbnail_alt'];

        # Vérification de l'unicité de l'url
        foreach(\$plxAdmin->plxGlob_arts->aFiles as \$numart => \$filename) {
            if(preg_match("/^_?[0-9]{4}.([0-9,|home|draft]*).[0-9]{3}.[0-9]{12}.".\$_POST["url"].".xml\$/", \$filename)) {
                if(\$numart!=str_replace('_', '',\$_POST['artId'])) {
                    \$valid = plxMsg::Error(L_ERR_URL_ALREADY_EXISTS." : ".plxUtils::strCheck(\$_POST["url"])) AND \$valid;
                    break;
                }
            }
        }
        # Vérification de la validité de la date de publication
        if(!plxDate::checkDate(\$_POST['date_publication_day'],\$_POST['date_publication_month'],\$_POST['date_publication_year'],\$_POST['date_publication_time'])) {
            \$valid = plxMsg::Error(L_ERR_INVALID_PUBLISHING_DATE) AND \$valid;
            break;
        }
        # Vérification de la validité de la date de creation
        if(!plxDate::checkDate(\$_POST['date_creation_day'],\$_POST['date_creation_month'],\$_POST['date_creation_year'],\$_POST['date_creation_time'])) {
            \$valid = plxMsg::Error(L_ERR_INVALID_DATE_CREATION) AND \$valid;
            break;
        }
        # Vérification de la validité de la date de mise à jour
        if(!plxDate::checkDate(\$_POST['date_update_day'],\$_POST['date_update_month'],\$_POST['date_update_year'],\$_POST['date_update_time'])) {
            \$valid = plxMsg::Error(L_ERR_INVALID_DATE_UPDATE) AND \$valid;
            break;
        }
        if(\$valid) {
            \$plxAdmin->editArticle(\$_POST,\$_POST['artId']);
        }
    }
    if(\$valid) {
        plxMsg::Info(L_ARTICLE_SAVE_SUCCESSFUL);
        ('$redirect' == 'index' ? header('Location: index.php') : header('Location: article.php?a='.\$_POST['artId']));
        exit;
    # Si url ou date invalide, on ne sauvegarde pas mais on repasse en mode brouillon
    }else{
        plxMsg::Error(L_ARTICLE_SAVE_ERR);
        header('Location: index.php');
        exit;
    }
}

    ob_start();
END;
        echo '<?php' . $string . '?>';
    }


    ########################################
    # AdminArticlePrepend
    ########################################
    # Description:
    public function AdminArticlePrepend(){

        $string = <<<END
        if(!empty(\$_POST)) {
            if (isset(\$_POST['title'], \$_POST['url'], \$_POST['catId'], \$_POST['artId']) && strpos(\$_POST['title'],\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_ART_DUPLICATED')) === false && in_array('draft',\$_POST['catId']) && substr(\$_POST['url'],-8) == '-copyart' && plxUtils::title2url(\$_POST['title']).'-copyart' != \$_POST['url']) {

                # On genère le nouveau nom de notre fichier
                \$time = \$_POST['date_publication_year'].\$_POST['date_publication_month'].\$_POST['date_publication_day'].substr(str_replace(':','',\$_POST['date_publication_time']),0,4);

                if(!preg_match('/^[0-9]{12}$/',\$time)) \$time = date('YmdHi'); # Check de la date au cas ou...
                if(empty(\$_POST['catId'])) \$_POST['catId'] = array('000'); # Catégorie non classée

                \$oldfilename = PLX_ROOT.\$plxAdmin->aConf['racine_articles'].\$_POST['url'].'.xml';
                \$_POST['url'] = plxUtils::title2url(\$_POST['title']);

                \$filename = PLX_ROOT.\$plxAdmin->aConf['racine_articles'].\$_POST['artId'].'.'.implode(',', \$_POST['catId']).'.'.trim(\$_POST['author']).'.'.\$time.'.'.\$_POST['url'].'.xml';
                
                # On va mettre à jour notre fichier
                if(rename(\$oldfilename,\$filename)) {
                    # mise à jour de la liste des tags
                    \$plxAdmin->aTags[\$_POST['artId']] = array('tags'=>trim(\$_POST['tags']), 'date'=>\$time, 'active'=>intval(!in_array('draft', \$_POST['catId'])));
                    \$plxAdmin->editTags();
                }
            }
        }
        
END;
        echo '<?php' . $string . '?>';

    }

    ########################################
    # AdminIndexPagination 
    ########################################
    # Description:
    public function AdminIndexPagination (){

        $string = <<<END
        \$c = ob_get_clean();        
        echo str_replace('<option value="delete">'.L_DELETE.'</option>','<option value="copy">'.\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_COPY').'</option><option value="delete">'.L_DELETE.'</option>',\$c);
        
END;
        echo '<?php' . $string . '?>';
    }

    ########################################
    # AdminArticleTop 
    ########################################
    # Description:
    public function AdminArticleTop (){

        $string = <<<END
        if(in_array('draft', \$catId) && strpos(\$title,\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_ART_DUPLICATED')) !== false) { # brouillon
            echo '<div class="info">'.\$plxAdmin->plxPlugins->aPlugins['$this->className']->getLang('L_REMINDER').'</div>';
        }
        
END;
        echo '<?php' . $string . '?>';
    }

}





/* Pense-bete:
 * Récuperer des parametres du fichier parameters.xml
 *  $this->getParam("<nom du parametre>")
 *  $this-> setParam ("param1", 12345, "numeric")
 *  $this->saveParams()
 *
 *  plxUtils::strCheck($string) : sanitize string
 *
 * 
 * Quelques constantes utiles: 
 * PLX_CORE
 * PLX_ROOT
 * PLX_CHARSET
 * PLX_PLUGINS
 * PLX_CONFIG_PATH
 * PLX_ADMIN (true si on est dans admin)
 * PLX_CHARSET
 * PLX_VERSION
 * PLX_FEED
 *
 * Appel de HOOK dans un thème
 *  eval($plxShow->callHook("ThemeEndHead","param1"))  ou eval($plxShow->callHook("ThemeEndHead",array("param1","param2")))
 *  ou $retour=$plxShow->callHook("ThemeEndHead","param1"));
 */